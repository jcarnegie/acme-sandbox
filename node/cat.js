import Promise from 'bluebird';
import { readFile } from 'fs';

const theFile = process.argv[2];

const catFile = (file) => new Promise(function (resolve, reject) {
  readFile(file, 'utf8', function(err, contents) {
    if (err) return reject(err);
    resolve(contents);
  });  
});

catFile(theFile)
  .then(function(contents) {
    console.log(contents);
    return contents;
  })
  .then(function(contents) {
    console.log('Hello World');
    console.log(contents);
  });

// console.err('Error reading file:', err);
// console.log(contents);

