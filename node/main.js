
import { readFile } from 'fs';
import http from 'http';

// next.js

const server = http.createServer();

server.listen(80, (req, res) => {
  // respond to requests
});

readFile();

// const obj = {
//   a: 1,
//   b: 2,
//   z: "string"
// };

// const arr = [1, 2, 3];

// // old way
// // const a = obj.a;
// const first = arr[0];
// const second = arr[1];
// const third = arr[2];


// // es6 way
// const { a, b, z } = obj;

// const [first, second, third] = arr;

// console.log(first);
// console.log(second);
// console.log(third);