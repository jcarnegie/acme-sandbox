

class Foo {
  constructor() {
    this.x = 1;
  }

  printX() {
    console.log(this.x);
  }
};

const foo = new Foo();

foo.printX();

const newPrintX = foo.printX.bind(foo);

newPrintX();