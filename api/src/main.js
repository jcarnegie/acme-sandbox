import { GraphQLServer } from 'graphql-yoga';
import { Client } from 'pg';

const main = async () => {
  const client = new Client({
    host: process.env.POSTGRES_HOST || 'localhost',
    user: 'postgres',
    password: 'password',
  });
  await client.connect();

  const typeDefs = `
    type User {
      id: ID!
      email: String!
    }

    type Account {
      id: ID!
      email: String!
    }

    type Query {
      hello(name: String): String!
      users: [User]
      accounts: [Account]
    }

    type Mutation {
      login(email: String!, password: String!): Boolean
    }
  `;

  const users = {
    'jeff.carnegie@gmail.com': {
      password: 'password'
    }
  }

  const resolvers = {
    Query: {
      hello: (_, { name }) => `Hello ${name || 'World'}`,
      users: () => ([
        { id: 1, email: 'dean@acme.com' }
      ]),
      accounts: async () => {
        const SQL = 'select * from accounts';
        return (await client.query(SQL)).rows;
      },
    },
    Mutation: {
      login: (_, { email, password }) => {
        if (users[email] && users[email].password === password) {
          return true;
        }
        return false;
      },
    }
  };

  const server = new GraphQLServer({ typeDefs, resolvers });
  const config = {
    cors: {
      "origin": "*",
      "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
      "preflightContinue": false,
      "optionsSuccessStatus": 204
    }
  };
  server.start(config, () => console.log('Server is running on localhost:4000'));
};

main();
