module.exports = function wallabyConfig(wallaby) {
  return {
    files: [
      'src/**/*.js',
    ],
    tests: [
      'test/**/*.test.js',
    ],
    env: {
      type: 'node',
    },
    compilers: {
      '**/*.js': wallaby.compilers.babel(),
    },
    setup: function babelRegeister() {
      // eslint-disable-next-line
      require('babel-register');
    },
  };
};
