/* eslint-disable no-unused-expressions,no-shadow */
/* @flow weak */
import React, { Component } from 'react';
import gql from 'graphql-tag';
import autobind from 'autobind-decorator';
import { Mutation } from 'react-apollo';
// import { map } from 'ramda';
import withApollo from '../lib/withApollo';
// import App from '../components/App';

const register = gql`
  mutation register($email: String!, $password: String!) {
    register(email: $email, password: $password)
  }
`;

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email:    '',
      password: '',
    };
  }

  @autobind
  handleSubmit(registerFn) {
    return async (e) => {
      e.preventDefault();
      await registerFn({
        variables: {
          email:    this.state.email,
          password: this.state.password,
        },
      });
      // route back to /
    };
  }

  @autobind
  updateForm(field) {
    return (e) => {
      console.log('updateForm', field, e.target.value);
      this.setState({ [field]: e.target.value });
    };
  }

  render() {
    return (
      <Mutation mutation={register}>
        {(register, { data }) => (
          <form onSubmit={this.handleSubmit(register)}>
            <label htmlFor="email">
              Email:
              <input id="email" type="text" value={this.state.email} onChange={this.updateForm('email')} />
            </label>
            <br />
            <label htmlFor="password">
              Password:
              <input id="password" type="password" value={this.state.password} onChange={this.updateForm('password')} />
            </label>
            <button type="submit">Register</button>
          </form>
        )}
      </Mutation>
    );
  }
}

export default withApollo(Register);
