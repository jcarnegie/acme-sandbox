/* eslint-disable no-unused-expressions,no-shadow */
/* @flow weak */
import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import gql from 'graphql-tag';
import autobind from 'autobind-decorator';
import { Mutation } from 'react-apollo';
// import { map } from 'ramda';
import withApollo from '../lib/withApollo';
// import App from '../components/App';

const login = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password)
  }
`;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email:    '',
      password: '',
    };
  }

  @autobind
  handleSubmit(loginFn) {
    return async (e) => {
      e.preventDefault();
      const result = await loginFn({
        variables: {
          email:    this.state.email,
          password: this.state.password,
        },
      });
      console.log(result);
      if (result.data.login) {
        // route back to /
        return Router.push('/');
      }
      // eslint-disable-next-line no-alert,no-undef
      return alert('Invalid username or password');
    };
  }

  @autobind
  updateForm(field) {
    return (e) => {
      console.log('updateForm', field, e.target.value);
      this.setState({ [field]: e.target.value });
    };
  }

  render() {
    return (
      <Mutation mutation={login}>
        {register => (
          <div>
            <Link href="/"><a>Home</a></Link>
            <form onSubmit={this.handleSubmit(register)}>
              <label htmlFor="email">
                Email:
                <input id="email" type="text" value={this.state.email} onChange={this.updateForm('email')} />
              </label>
              <br />
              <label htmlFor="password">
                Password:
                <input id="password" type="password" value={this.state.password} onChange={this.updateForm('password')} />
              </label>
              <button type="submit">Login</button>
            </form>
          </div>
        )}
      </Mutation>
    );
  }
}

export default withApollo(Login);
