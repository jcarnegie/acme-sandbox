import { map, reduce, toString } from 'ramda';

export const add = (a, b) => a + b;

export const multiply = (a, b) => a * b;

export const subtract = (a, b) => a - b;

export const greetMe = name => `Hello, ${name}!`;

export const objectWithFooProp = () => ({ foo: 'bar' });

export const sum = (numbers) => {
  // let total = 0;
  // for( let i = 0; i < numbers.length; i++ ) {
  //   total += numbers[i];
  // }
  // return total;

  return reduce(add, 0, numbers);
};

export const stringify = numbers => {
  // const arr = [];
  // for (let i = 0; i < numbers.length; i++) {
  //   arr.push(numbers[i].toString());
  // }
  // return arr;
  return map(toString, numbers);
};