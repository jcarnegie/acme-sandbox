
import {
  add,
  multiply,
  subtract,
  greetMe,
  objectWithFooProp,
  sum,
  stringify,
} from '../src/main';

test('should add two numbers', () => {
  const result = add(1, 2);
  expect(result).toEqual(3);
});

test('should multiply two numbers', () => {
  const result = multiply(3, 10);
  expect(result).toEqual(30);
});

test('should subtract two numbers', () => {
  const result = subtract(10, 7);
  expect(result).toEqual(3);
});

test('should greet me', () => {
  const result = greetMe('Jeff');
  expect(result).toEqual('Hello, Jeff!');
});

test('should have property "foo"', () => {
  const result = objectWithFooProp();
  expect(result).toHaveProperty('foo');
});

test('should compute sum of array of numbers', () => {
  const result = sum([1, 2, 3]);
  expect(result).toEqual(6);
});

test('should stringify list of numbers', () => {
  const result = stringify([1, 2, 3]);
  expect(result).toEqual(['1', '2', '3']);
});